from django.contrib.sites.shortcuts import get_current_site
from django.db.models import Q
from django.http import Http404
from django.http import HttpRequest
from django.http import HttpResponse
from django.shortcuts import redirect as djredir
from django.shortcuts import render

from .models import URL


def redirect(request: HttpRequest, id: str) -> HttpResponse:
    host = request.get_host()
    url = URL.objects.filter(
        Q(domain__hostname=host) | Q(domain__isnull=True), short_name=id
    ).first()
    if not url:
        raise Http404
    return djredir(url.target)


def index(request: HttpRequest) -> HttpResponse:
    host = request.get_host()
    site = get_current_site(request)
    if host != site.domain:
        # If the host isn't the main host, redirect.
        try:
            return redirect(request, "")
        except Http404:
            # If there's no URL, show the main page.
            pass
    return render(request, "index.html")
