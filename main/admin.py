from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from djangoql.admin import DjangoQLSearchMixin

from .models import Domain
from .models import Organization
from .models import URL
from .models import User

admin.site.register(User, UserAdmin)


@admin.register(URL)
class URLAdmin(DjangoQLSearchMixin, admin.ModelAdmin):
    list_display = [
        "id",
        "short_name",
        "target",
        "organization",
        "timestamp",
        "user",
        "domain",
    ]
    list_filter = ("timestamp", "organization__name", "domain__hostname")
    search_fields = ("short_name", "target")


@admin.register(Organization)
class OrganizationAdmin(DjangoQLSearchMixin, admin.ModelAdmin):
    list_display = ["name"]
    search_fields = ("name",)


@admin.register(Domain)
class DomainAdmin(DjangoQLSearchMixin, admin.ModelAdmin):
    list_display = ["hostname", "organization"]
    search_fields = ("hostname",)
