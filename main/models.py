from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from shortuuid.django_fields import ShortUUIDField


class Organization(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class User(AbstractUser):
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)


class Domain(models.Model):
    hostname = models.CharField(max_length=5000, unique=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)

    def __str__(self):
        return self.hostname


class URL(models.Model):
    short_name = ShortUUIDField(
        alphabet="abcdefghijkmnopqrstuvwxyz",
        length=5,
        max_length=200,
        db_index=True,
        blank=True,
    )
    target = models.URLField(max_length=20000)
    timestamp = models.DateTimeField(auto_now_add=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    domain = models.ForeignKey(Domain, on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "URL"
        unique_together = [["short_name", "domain"]]

    def get_absolute_url(self):
        return reverse("main:redirect", args=[self.id])

    def __str__(self):
        return self.short_name
